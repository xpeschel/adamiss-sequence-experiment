package cz.muni.disa.adamiss.se.stats.collectors;

import cz.muni.disa.adamiss.core.objects.AdamissObject;
import cz.muni.disa.adamiss.core.objects.Transaction;

import java.util.HashMap;
import java.util.Map;

public class RepeatibilityCounter {

    private int min = Integer.MAX_VALUE;
    private double avg = 0f;
    private int max = Integer.MIN_VALUE;
    private double uniqueItemRepeatedCount = 0;
    private int counter = 0;

    public void processTransaction(Transaction transaction) {
        Map<AdamissObject, Integer> uniqueItemCounter = new HashMap<>();
        counter++;

        for (AdamissObject item : transaction.getData()) {
            int count = uniqueItemCounter.getOrDefault(item, 0) + 1;
            uniqueItemCounter.put(item, count);
        }

        var repeatedItems = uniqueItemCounter.keySet().stream().filter((item) -> uniqueItemCounter.get(item) > 1).count();
        var localMin = uniqueItemCounter.values().stream().min(Integer::compareTo).get();
        var localMax = uniqueItemCounter.values().stream().max(Integer::compareTo).get();
        var localAvg = uniqueItemCounter.values().stream().mapToInt(item->item).average().getAsDouble();
        min = Math.min(min, localMin);
        max = Math.max(max, localMax);
        avg = (avg + ((localAvg-avg)/(double) counter));
        uniqueItemRepeatedCount = uniqueItemRepeatedCount + ((repeatedItems - uniqueItemRepeatedCount) / (double) counter);
    }

    public int getMin() {
        return min;
    }

    public double getAvg() {
        return avg;
    }

    public int getMax() {
        return max;
    }

    public double getUniqueItemRepeatedCount() {
        return uniqueItemRepeatedCount;
    }
}
