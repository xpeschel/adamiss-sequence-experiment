package cz.muni.disa.adamiss.se.loader;

import cz.muni.disa.adamiss.core.objects.AdamissObject;
import cz.muni.disa.adamiss.core.objects.Transaction;
import cz.muni.disa.adamiss.core.objects.TransactionFile;
import cz.muni.disa.adamiss.core.objects.impl.AdamissObjectImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DataLoader {
    public static Logger logger = LogManager.getLogger(DataLoader.class);
    /**
     * Function to read simple data file into TransactionFile
     *
     * @return TransactionFile with all transactions
     */
    public static TransactionFile loadDataset(String path) {
        logger.debug("Loading: " + path);
        final List<Transaction> transactions = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(DataLoader.class.getClassLoader().getResourceAsStream(path)))) {
            reader.lines().forEach(
                    (line) -> {
                        logger.trace(line);
                        String[] strings = line.split(" ");
                        List<AdamissObject> objects = new ArrayList<>();
                        for (String s : strings) {
                            objects.add(new AdamissObjectImpl(s));

                        }
                        transactions.add(new Transaction(objects));
                    }
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        TransactionFile database = new TransactionFile(transactions);

        return database;
    }


}
