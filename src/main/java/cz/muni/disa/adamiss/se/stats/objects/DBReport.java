package cz.muni.disa.adamiss.se.stats.objects;

import java.util.Collections;
import java.util.List;

public record DBReport(
        String name,
        int size,
        int uniqueItems,
        int minLength,
        double avgLength,
        int maxLength,
        int minRepeatibility,
        double avgRepeatibility,
        int maxRepeatibility,
        double avgRepeatedItems,
        List<Integer> uniqueItemDistribution
) {
    public DBReport {
        Collections.sort(uniqueItemDistribution, Collections.reverseOrder());
        uniqueItemDistribution = List.copyOf(uniqueItemDistribution);
    }


    public static String getHeader(){
        return "name, " +
                "size, " +
                "uniqueItems, " +
                "minLength, " +
                "avgLength, " +
                "maxLength, " +
                "minRepeatibility, " +
                "avgRepeatibility, " +
                "maxRepeatibility, " +
                "avgRepeatedItems";
    }
    public static String toCSVWithHeader(List<DBReport> reports) {
        StringBuilder dbReportCSV = new StringBuilder(getHeader()).append(System.lineSeparator());
        for (DBReport report : reports) {
            dbReportCSV.append(report.toCSV()).append(System.lineSeparator());
        }
        return dbReportCSV.toString();
    }

    public String toCSV() {
        return this.name() + "," +
                this.size() + "," +
                this.uniqueItems() + "," +
                this.minLength + "," +
                this.avgLength + "," +
                this.maxLength + "," +
                this.minRepeatibility + "," +
                this.avgRepeatibility + "," +
                this.maxRepeatibility + "," +
                this.avgRepeatedItems + "," /* +
                "\"" + this.uniqueItemDistribution().stream().map(item -> item.toString()).collect(Collectors.joining(",")) + "\""*/;
    }
}
