package cz.muni.disa.adamiss.se.loader;

import cz.muni.disa.adamiss.core.objects.TransactionFile;
import cz.muni.disa.adamiss.se.common.Dataset;
import javafx.util.Pair;

import java.util.Iterator;
import java.util.List;

public class SingleDatasetLoaderImpl extends DatasetLoader {

    final Iterator<Dataset> datasets;

    public SingleDatasetLoaderImpl(Dataset dataset) {
        datasets = List.of(dataset).iterator();
    }

    @Override
    public boolean hasNext() {
        return datasets.hasNext();
    }

    @Override
    public Pair<String, TransactionFile> next() {
        return getDataset(datasets.next());
    }
}
