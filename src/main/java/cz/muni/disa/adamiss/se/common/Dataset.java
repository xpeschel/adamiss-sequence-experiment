package cz.muni.disa.adamiss.se.common;

public enum Dataset {
    BIBLE("datasets/bible.txt", new int[]{8192,4096,2048/*,1024,512,384,256*/}),
    COVID("datasets/covid.txt", new int[]{256,128,64/*,32,16,8,4*/}),
    GENERATED1("datasets/generated.txt", new int[]{512,256,128/*,96,90,80,64*/}),
    GENERATED2("datasets/generated2.txt", new int[]{512,256,128/*,112,96,80,64*/}),
    GENERATED3("datasets/generated3.txt", new int[]{512,256,124/*,112,96,80,64*/}),
    KDDCUP("datasets/kddcup.txt", new int[]{2048,1024,768/*,700,512,256,128*/}),
    LEVIATHAN("datasets/leviathan.txt", new int[]{2048,1024,512/*,256,128,64,32*/}),
    SIGN("datasets/sign.txt", new int[]{512,256,128/*,64,32,16,8,4*/});

    private final String path;
    private final int[] supports;

    Dataset(String path, int[] supports) {
        this.path = path;
        this.supports = supports;
    }

    public String path() {
        return path;
    }

    public int[] getSupports() {
        return this.supports;
    }

    @Override
    public String toString() {
        return path;
    }
}
