package cz.muni.disa.adamiss.se.structures;

import cz.muni.disa.adamiss.core.objects.AdamissObject;
import cz.muni.disa.adamiss.core.objects.Transaction;
import cz.muni.disa.adamiss.core.objects.TransactionFile;
import javafx.util.Pair;

import java.util.*;

public class VerticalDatabase {

    private Map<List<AdamissObject>, List<Pair<Long, Long>>> database = new HashMap<>();

    public VerticalDatabase(TransactionFile file){
        List<Triplet> occurrences = new ArrayList<>();
        Long i = 0L;
        for (Transaction t: file.getTransactions()){
            Set<AdamissObject> setOfObj = new HashSet<>(t.getData());
            for(AdamissObject o: setOfObj){
                for(Long j = 0L; j< t.getData().size(); j++){
                    if(o.equals(t.getData().get(Math.toIntExact(j)))){
                        occurrences.add(new Triplet(o,i,j));
                    }
                }
            }
            i++;
        }

        for(Triplet t: occurrences){
            Pair<Long, Long> entry = new Pair<Long, Long>((Long) t.getSecond(), (Long) t.getThird());
            List<AdamissObject> pattern = new ArrayList<>();
            pattern.add((AdamissObject) t.getFirst());
            if(database.containsKey(pattern)){
                database.get(pattern).add(entry);
            }
            else {
                List<Pair<Long,Long>> l = new ArrayList<>();
                l.add(entry);
                database.put(pattern, l);
            }
        }

    }

    public Map<List<AdamissObject>, List<Pair<Long, Long>>> getDatabase(){
        return database;
    }
}
