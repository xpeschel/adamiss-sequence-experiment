package cz.muni.disa.adamiss.se.loader;

import cz.muni.disa.adamiss.core.objects.TransactionFile;
import cz.muni.disa.adamiss.se.common.Dataset;
import javafx.util.Pair;

import java.util.Arrays;
import java.util.Iterator;

public class AllDatasetsLoaderImpl extends DatasetLoader {

    Iterator<Dataset> datasets = Arrays.asList(Dataset.values()).iterator();

    @Override
    public boolean hasNext() {
        return datasets.hasNext();
    }

    @Override
    public Pair<String, TransactionFile> next() {
        return getDataset(datasets.next());
    }
}
