package cz.muni.disa.adamiss.se.structures;

import cz.muni.disa.adamiss.core.objects.TransactionFile;
import cz.muni.disa.adamiss.se.common.Dataset;
import cz.muni.disa.adamiss.se.stats.objects.DBReport;

public record DatasetContainer(String name, TransactionFile dataset, DBReport dbReport) {
}
