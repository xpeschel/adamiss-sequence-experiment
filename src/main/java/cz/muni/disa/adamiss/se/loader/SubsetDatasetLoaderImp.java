package cz.muni.disa.adamiss.se.loader;

import cz.muni.disa.adamiss.core.objects.TransactionFile;
import cz.muni.disa.adamiss.se.Main;
import cz.muni.disa.adamiss.se.common.Dataset;
import cz.muni.disa.adamiss.se.generators.SubsetGenerator;
import javafx.util.Pair;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.function.Consumer;

public class SubsetDatasetLoaderImp extends DatasetLoader{

    Queue<RunSetting> datasetQueue = new LinkedList();

    public SubsetDatasetLoaderImp(List<Dataset> datasets, int amount, boolean size, boolean unique, boolean length, boolean combined,  Random random){

        Iterator<Dataset> iterator = new ArrayList<>(datasets).iterator();
        while(iterator.hasNext()){
            Dataset dataset = iterator.next();
            if(size) {
                for (int i = 0; i < amount; i++)
                    datasetQueue.add(new RunSetting(dataset, RunType.SIZE, random, i));
            }
            if (unique){
                for (int i = 0; i < amount; i++)
                    datasetQueue.add(new RunSetting(dataset, RunType.UNIQUE, random, i));
            }
            if(length){
                for (int i = 0; i < amount; i++)
                    datasetQueue.add(new RunSetting(dataset, RunType.LENGTH, random, i));
            }
            if(combined){
                for (int i = 0; i < amount; i++)
                    datasetQueue.add(new RunSetting(dataset, RunType.COMBINED, random, i));
            }
        }
    }

    @Override
    public boolean hasNext() {
        return !datasetQueue.isEmpty();
    }

    @Override
    public Pair<String, TransactionFile> next() {
        RunSetting setting = datasetQueue.poll();
        Pair<String, TransactionFile> dataset = getDataset(setting.sourceDataset());

        Pair<String, TransactionFile> result = null;
        switch(setting.runType()) {
            case SIZE ->{result = SubsetGenerator.generateSizeSubset(dataset,setting.random(), setting.index());}
            case UNIQUE ->{result = SubsetGenerator.generateUniqueSubset(dataset,setting.random(), setting.index());}
            case LENGTH ->{result = SubsetGenerator.generateLengthSubset(dataset,setting.random(), setting.index());}
            case COMBINED ->{result = SubsetGenerator.generateCombinedSubset(dataset,setting.random(), setting.index());}
        }

        return result;
    }



}

enum RunType{
    SIZE,
    UNIQUE,
    LENGTH,
    COMBINED;
}

record RunSetting(Dataset sourceDataset, RunType runType, Random random, int index){}


