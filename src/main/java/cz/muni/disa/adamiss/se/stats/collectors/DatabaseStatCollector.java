package cz.muni.disa.adamiss.se.stats.collectors;

import cz.muni.disa.adamiss.core.objects.Transaction;
import cz.muni.disa.adamiss.core.objects.TransactionFile;
import cz.muni.disa.adamiss.se.common.Dataset;
import cz.muni.disa.adamiss.se.stats.objects.DBReport;

public class DatabaseStatCollector {

    public DBReport collect(String dataset, TransactionFile database) {
        UniqueItemCounter uniqueItemCounter = new UniqueItemCounter();
        RepeatibilityCounter repeatibilityCounter = new RepeatibilityCounter();
        int minLength = Integer.MAX_VALUE;
        float avgLength = 0f;
        int maxLength = Integer.MIN_VALUE;
        int n = 0;

        for (Transaction transaction : database.getTransactions()) {
            n += 1;
            var length = transaction.getData().size();
            minLength = Math.min(minLength, length);
            maxLength = Math.max(maxLength, length);
            avgLength = avgLength + ((length - avgLength) / n);
            uniqueItemCounter.processTransaction(transaction);
            repeatibilityCounter.processTransaction(transaction);

        }

        return new DBReport(
                dataset,
                database.getTransactions().size(),
                uniqueItemCounter.getUniqueItemCount(),
                minLength,
                avgLength,
                maxLength,
                repeatibilityCounter.getMin(),
                repeatibilityCounter.getAvg(),
                repeatibilityCounter.getMax(),
                repeatibilityCounter.getUniqueItemRepeatedCount(),
                uniqueItemCounter.getUniqueItemDistribution());
    }


}
