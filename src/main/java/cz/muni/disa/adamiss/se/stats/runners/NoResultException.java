package cz.muni.disa.adamiss.se.stats.runners;

public class NoResultException extends Exception {
    public NoResultException(String s) {
        super(s);
    }
    public NoResultException(String s, Throwable t) {
        super(s,t);
    }
}
