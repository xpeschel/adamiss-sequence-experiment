package cz.muni.disa.adamiss.se.stats.collectors;

import cz.muni.disa.adamiss.core.objects.AdamissObject;
import cz.muni.disa.adamiss.core.objects.Transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UniqueItemCounter {

    private final HashMap<AdamissObject, Integer> itemSet = new HashMap<>();

    public void processTransaction(Transaction transaction) {
        for (AdamissObject item : transaction.getData()) {
            var count = itemSet.getOrDefault(item, 0);
            itemSet.put(item, count+1);
        }
    }

    public int getUniqueItemCount() {
        return itemSet.size();
    }

    public List<Integer> getUniqueItemDistribution() {
        return new ArrayList<>(itemSet.values());
    }
}
