package cz.muni.disa.adamiss.se.loader;

import cz.muni.disa.adamiss.core.objects.TransactionFile;
import cz.muni.disa.adamiss.se.common.Dataset;
import javafx.util.Pair;

import java.util.Iterator;

public abstract class DatasetLoader implements Iterator<Pair<String, TransactionFile>> {

    protected static Pair<String, TransactionFile> getDataset(Dataset dataset){
        return new Pair<>(dataset.name(), DataLoader.loadDataset(dataset.path()));
    }

}
