from sklearn import tree
from sklearn import model_selection
from sklearn import metrics
import numpy as np
import csv
import sys
import pickle


def load_dt(tree_path):
    return pickle.load(tree_path)


def conv(str_val):
    try:
        return float(str_val)
    except ValueError:
        return str_val


def process(datafile_path):
    with open(datafile_path, newline='') as csvfile:
        reader = csv.reader(csvfile)
    try:
        data = []
        for row in reader:
            raw = [conv(x) for x in row]
            data.append(raw)
    except csv.Error as e:
        sys.exit("Error {} file {}".format(e, datafile_path))


def predict(dtree, data):
    pass


def  sumup(data, method):
    pass


def main():
    tree_path = sys.argv[1]
    dtree = load_dt(tree_path)
    datafile_path = sys.argv[2]
    data = process(datafile_path)
    opt_time = predict(dtree, data)
    gsp_time = sumup(data, "GSP")
    prefix_time = sumup(data, "PREFIX_SPAN")
    spam_time = sumup(data, "SPAM")
    print("opt: {}, gsp: {}, prefix_span: {}, spam: {}", opt_time, gsp_time, prefix_time, spam_time)


if __name__ == '__main__':
    main()